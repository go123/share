#获取下载地址
import requests
import json
import datetime
import random
# import urllib

# 说明：因为这段代码是平时自己用的，所以写的很简陋，见谅哈
# 这段代码的目的是下载我在金山文档里的一个Excel表，这个表当时被我设置了共享编辑，我用kettle配置的5分钟下载一次，这样就能实现伪实时同步了
# 下面会多次提及抓包结果，其实就是指的Chrome浏览器的开发者工具的【网络】面板，每次请求都会展示请求参数以及header头，抓包结果就是指的请求参数和header头，术语不太标准，见谅

s = requests.Session()
headers = {
    'origin': 'https://www.kdocs.cn',

    # 这里之所以有两个referer参数，是因为我发现我的两个表在下载的时候，通过开发者工具抓包拿到的referer其实是不一样的，可能是wps的问题吧，不管了，按照抓包的结果选吧
    # xxxxxx其实是代表了一串数字或者固定的字母数字组合，具体以抓包结果为准
    # 'referer': 'https://www.kdocs.cn/view/p/xxxxxx?iframe=weboffice',
    'referer': 'https://www.kdocs.cn/view/l/xxxxxx?iframe=weboffice',

    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36',
    'accept': '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
}

cookies = {}

cookie_str也是以header头里面的cookies字符串为准，直接复制就行，下面几行代码是对这串字符串进行拆分的
cookie_str = 'header头里面的cookies字符串的内容，直接复制过来就行'
cookie_str = cookie_str.replace(' ','')
for i in cookie_str.split(';'):
    cookies[i.split('=')[0]] = i.split('=')[1]

data = {
    'isblocks': 'false'
}
# 下面的url以实际的抓包时显示的请求url为准，url形式供参考
ret3 = s.get(url = 'https://drive.kdocs.cn/api/v3/groups/xxxxxxxxx/files/zzzzzzzzzz/download?isblocks=false',headers = headers,cookies = cookies,data = data)
rettext = json.loads(ret3.text)
# 这里之所以会用两种形式获取rettext，其实也是因为我的两个表wps给了两种返回结果，导致我得适配它的结果，具体你可以以抓包的返回json的结构为准
# rettext = rettext['fileinfo']['static_url']
rettext = rettext['fileinfo']['url']
rettext

#通过urllib下载文件

import urllib

url = rettext
f = urllib.request.urlopen(url)
data = f.read()
# 存储位置可自定义
# 我是在windows下执行的，所以路径是windows的路径，建议放绝对路径，这其实就是python自己的语法了，献丑了
with open("D:/你的文件存放路径/文件名.xlsx", 'wb') as code:
    ret_write_status = code.write(data)
    
if(ret_write_status > 0):
    print('success：文件写入成功')
else:
    print('failed：文件写入失败')

# 上面的代码其实是我用jupyter notebook一段一段写出来的，所以在第49行的时候还单独加载了urllib，其实就是上面的逻辑都调试好了，临时百度出来的可以用urllib下载文件，嘿嘿，反正够用就行